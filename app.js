let from_currencyEl = document.getElementById("from_currency");
let from_ammountEl = document.getElementById("from_ammount");
let to_currencyEl = document.getElementById("to_currency");
let to_ammountEl = document.getElementById("to_ammount");
let rateEl = document.getElementById("rate");
let exchange = document.getElementById("exchange");

from_currencyEl.addEventListener("change", calculate);
from_ammountEl.addEventListener("input", calculate);
to_currencyEl.addEventListener("change", calculate);
to_ammountEl.addEventListener("input", calculate);

exchange.addEventListener("click", () => {
  let temp = from_currencyEl.value;
  from_currencyEl.value = to_currencyEl.value;
  to_currencyEl.value = temp;
  calculate();
});

function calculate() {
  let from_currency = from_currencyEl.value;
  let to_currency = to_currencyEl.value;

  fetch(`https://api.exchangerate-api.com/v4/latest/${from_currency}`)
    .then((res) => res.json())
    .then((res) => {
      let rate = res.rates[to_currency];
      rateEl.innerText = `1 ${from_currency} = ${rate} ${to_currency}`;
      to_ammountEl.value = (from_ammountEl.value * rate).toFixed(2);
    });
}
function filter() {
  var keyword = document.getElementById("search").value;
  var select = document.getElementById("from_currency");
  for (var i = 0; i < select.length; i++) {
    var txt = select.options[i].text;
    if (!txt.match(keyword)) {
      $(select.options[i]).attr("disabled", "disabled").hide();
    } else {
      $(select.options[i]).removeAttr("disabled").show();
    }
  }
}

function filter2() {
  var keyword2 = document.getElementById("search2").value;
  var select2 = document.getElementById("to_currency");
  for (var i = 0; i < select2.length; i++) {
    var txt = select2.options[i].text;
    if (!txt.match(keyword2)) {
      $(select2.options[i]).attr("disabled", "disabled").hide();
    } else {
      $(select2.options[i]).removeAttr("disabled").show();
    }
  }
}
